# SailfishOS on Pine Don't be evil devkit (pine-dontbeevil)

![SailfishOS Logo](https://sailfishos.org/themes/sailfish/src/images/SailfishOS_blk.svg)


This is a work in progress rootfs for the pine-dontbeevil

Packages are maintained here:

- https://build.merproject.org/project/show/nemo:devel:hw:pine:dontbeevil
- https://github.com/sailfish-on-dontbeevil


Wiki: https://wiki.merproject.org/wiki/Adaptations/PinePhone64